#!/usr/bin/perl

use strict;
use warnings;

use Template;

my $template = Template->new(
    {   EVAL_PERL    => 1,
	  ABSOLUTE     => 1,
	  FILTERS => {},
    }
) or die Template->error();

open (SCORES, 'scores.txt');
my $vars;
my @results;
my $totalpatch;
my $totalqa;
my $totalrescued;
my $totalsigned;
my $totalfailed;
my $totalkittens = 0;
while (my $line = <SCORES>) {
    my ($name, $patches, $signedoff, $qa, $rescued, $failed ) = split(/\t/, $line);
    
    # calculate the kittens
    my $kittens = $patches + $signedoff + $qa + ($rescued * 2) + $failed;
    push @results, {'name' => $name, 
	  'patches' => $patches,
	  'signedoff' => $signedoff,
	  'rescued' => $rescued,
	  'qa' => $qa,
	  'kittens' => $kittens,
    'failed' => $failed
    };
    $totalkittens += $kittens;
    $totalpatch += $patches;
    $totalqa += $qa;
    $totalrescued += $rescued;
    $totalsigned += $signedoff;
    $totalfailed += $failed;
}

@results = sort { $b->{'kittens'} <=> $a->{'kittens'} } @results;
$vars = { 'results' => \@results, 'total' => $totalkittens , 'patch' => $totalpatch, 'qa' => $totalqa, 'rescued' => $totalrescued, 'signed' => $totalsigned, 'failed' => $totalfailed };

$template->process( 'templates/index.tt', $vars, 'index.html') || die Template->error();
